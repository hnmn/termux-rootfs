# -*- encoding: utf-8 -*-
# stub: fivemat 1.3.5 ruby lib

Gem::Specification.new do |s|
  s.name = "fivemat".freeze
  s.version = "1.3.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Tim Pope".freeze]
  s.date = "2017-06-02"
  s.description = "MiniTest/RSpec/Cucumber formatter that gives each test file its own line of dots".freeze
  s.email = ["code@tpope.net".freeze]
  s.homepage = "https://github.com/tpope/fivemat".freeze
  s.rubygems_version = "2.6.11".freeze
  s.summary = "Why settle for a test output format when you could have a test output fivemat?".freeze

  s.installed_by_version = "2.6.11" if s.respond_to? :installed_by_version
end
