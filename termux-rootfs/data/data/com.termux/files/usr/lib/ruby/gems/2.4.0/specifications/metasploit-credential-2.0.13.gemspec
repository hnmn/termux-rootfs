# -*- encoding: utf-8 -*-
# stub: metasploit-credential 2.0.13 ruby app/models app/validators lib

Gem::Specification.new do |s|
  s.name = "metasploit-credential".freeze
  s.version = "2.0.13"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["app/models".freeze, "app/validators".freeze, "lib".freeze]
  s.authors = ["Luke Imhoff".freeze, "Trevor Rosen".freeze]
  s.date = "2017-09-13"
  s.description = "The Metasploit::Credential namespace and its ActiveRecord::Base subclasses".freeze
  s.email = ["luke_imhoff@rapid7.com".freeze, "trevor_rosen@rapid7.com".freeze]
  s.homepage = "https://github.com/rapid7/metasploit-credential".freeze
  s.licenses = ["BSD-3-clause".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.2.0".freeze)
  s.rubygems_version = "2.6.11".freeze
  s.summary = "Credential models for metasploit-framework and Metasploit Pro".freeze

  s.installed_by_version = "2.6.11" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<metasploit-concern>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<metasploit_data_models>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<metasploit-model>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<railties>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<rubyntlm>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<rubyzip>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<rex-socket>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<pg>.freeze, [">= 0"])
    else
      s.add_dependency(%q<metasploit-concern>.freeze, [">= 0"])
      s.add_dependency(%q<metasploit_data_models>.freeze, [">= 0"])
      s.add_dependency(%q<metasploit-model>.freeze, [">= 0"])
      s.add_dependency(%q<railties>.freeze, [">= 0"])
      s.add_dependency(%q<rubyntlm>.freeze, [">= 0"])
      s.add_dependency(%q<rubyzip>.freeze, [">= 0"])
      s.add_dependency(%q<rex-socket>.freeze, [">= 0"])
      s.add_dependency(%q<pg>.freeze, [">= 0"])
    end
  else
    s.add_dependency(%q<metasploit-concern>.freeze, [">= 0"])
    s.add_dependency(%q<metasploit_data_models>.freeze, [">= 0"])
    s.add_dependency(%q<metasploit-model>.freeze, [">= 0"])
    s.add_dependency(%q<railties>.freeze, [">= 0"])
    s.add_dependency(%q<rubyntlm>.freeze, [">= 0"])
    s.add_dependency(%q<rubyzip>.freeze, [">= 0"])
    s.add_dependency(%q<rex-socket>.freeze, [">= 0"])
    s.add_dependency(%q<pg>.freeze, [">= 0"])
  end
end
