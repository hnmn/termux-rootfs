Generate SSH host keys with commands:

  $ ssh-keygen -t ed25519 -f host_ed25519 -N ''
  $ ssh-keygen -t rsa -b 4096 -f host_rsa -N ''
