==============================================================================
Quick Start
==============================================================================

Place your rootfs for RISC-V Linux here and adjust path to it in riscvemu.cfg.

Then you can run command 'riscv-linux'.

For more information, see manual riscvemu(1).
