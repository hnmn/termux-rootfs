package !{!APP_PACKAGE!}!;

import android.app.Activity;
import android.os.Bundle;
import !{!APP_PACKAGE!}!.R;

public class MainActivity extends Activity {
    protected void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.main);
    }
}
