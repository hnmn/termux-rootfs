ECJ's default classpath
=======================

If you want to add some custom jars to ECJ's classpath, place them here.

If you want to use a custom Android SDK JAR, replace symlink 'android.jar'
with your file. But make sure that there is no multiple Android SDKs in the
same classpath.
