QEMU (system-x86) emulator for Android
======================================

* Limbo_x86.apk - android application file of the QEMU

* TinyCore_8.2.iso - remastered TinyCore Linux distribution

  Includes tools such as Xvesa, GCC, MicroPython, Nano, Lynx,
  and much more. On boot, services SSH and Nginx are started
  automatically.

  Login credentials:

    User: tc
    Password: tcuser

* FreeDOS.img - Free DOS

  Includes Nasm, VIM, debug.com, some games and demos.
