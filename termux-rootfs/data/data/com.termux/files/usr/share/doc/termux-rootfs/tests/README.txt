Various test scripts for Termux-RootFS
======================================

* check-clang.sh       - test if clang is usable
* check-colors.sh      - test if your terminal supports color output
* check-fifo.sh        - test if FIFO can be created
* check-gcc.sh         - test if GCC is usable
* check-hardlinks.shn  - test if hardlinks are supported (may fail if SELinux is enabled)
* check-java.sh        - test if ECJ, DX, OpenJDK-JRE, DalvikVM are usable
* check-libs.sh        - test if all needed shared libraries are present
* check-rustc.sh       - test if Rust compiler is usable
* check-socket.sh      - test if Unix socket can be created
* check-su.sh          - test if superuser access is working
* check-symlinks.sh    - test if symlinks are supported
