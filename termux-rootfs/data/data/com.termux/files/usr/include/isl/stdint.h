#ifndef _ISL_INCLUDE_ISL_STDINT_H
#define _ISL_INCLUDE_ISL_STDINT_H 1
#ifndef _GENERATED_STDINT_H
#define _GENERATED_STDINT_H "isl 0.18"
/* generated using gnu compiler Android clang version 3.8.256229  (based on LLVM 3.8.256229) */
#define _STDINT_HAVE_STDINT_H 1
#include <stdint.h>
#endif
#endif
